<?php
if ($_POST) { 
	switch ($_POST['form_name']) {
		case 'home':{
			$variation = (empty(htmlspecialchars($_POST["variation"]))) ? "" : htmlspecialchars($_POST["variation"]);
			$filmName = (empty(htmlspecialchars($_POST["filmName"]))) ? "" : htmlspecialchars($_POST["filmName"]);
			$filmmaker = (empty(htmlspecialchars($_POST["filmmaker"]))) ? "" : htmlspecialchars($_POST["filmmaker"]);
			$cast = (empty(htmlspecialchars($_POST["cast"]))) ? "" : htmlspecialchars($_POST["cast"]);
			$genre = (empty(htmlspecialchars($_POST["genre"]))) ? "" : htmlspecialchars($_POST["genre"]);
			$timing = (empty(htmlspecialchars($_POST["timing"]))) ? "" : htmlspecialchars($_POST["timing"]);
			$description = (empty(htmlspecialchars($_POST["description"]))) ? "" : htmlspecialchars($_POST["description"]);
			$addMaterials = (empty(htmlspecialchars($_POST["addMaterials"]))) ? "" : htmlspecialchars($_POST["addMaterials"]);
			$name = (empty(htmlspecialchars($_POST["name"]))) ? "" : htmlspecialchars($_POST["name"]);
			$email = (empty(htmlspecialchars($_POST["email"]))) ? "" : htmlspecialchars($_POST["email"]);

			$emailgo= new TEmail; 
			$emailgo->from_email= ''; //email отправителя, можно оставить пустым
			$emailgo->from_name= 'Форма обратной связи homa page'; //Тема сообщения
			$emailgo->to_email= ''; // email получателя
			$emailgo->to_name= $name;

			$emailgo->info = array(
				'variation' => $variation,
				'filmName' => $filmName,
				'filmmaker' => $filmmaker,
				'cast' => $cast,
				'genre' => $genre,
				'timing' => $timing,
				'description' => $description,
				'addMaterials' => $addMaterials,
				'name' => $name,
				'email' => $email
			);


			$body = "";
			if($emailgo->info['variation'] != '') $body .= "Категория: ".$emailgo->info['variation']."\n";
			if($emailgo->info['filmName'] != '') $body .= "Название фильма: ".$emailgo->info['filmName']."\n";
			if($emailgo->info['cast'] != '') $body .= "В ролях: ".$emailgo->info['cast']."\n";
			if($emailgo->info['genre'] != '') $body .= "Жанр: ".$emailgo->info['genre']."\n";
			if($emailgo->info['timing'] != '') $body .= "Хронометраж: ".$emailgo->info['timing']."\n";
			if($emailgo->info['description'] != '') $body .= "Описание: ".$emailgo->info['description']."\n";
			if($emailgo->info['addMaterials'] != '') $body .= "Дополнительные материалы: ".$emailgo->info['addMaterials']."\n";
			if($emailgo->info['name'] != '') $body .= "Имя: ".$emailgo->info['name']."\n";
			if($emailgo->info['email'] != '') $body .= "Электронная почта: ".$emailgo->info['email']."\n";

			$emailgo->body= $body;
			$emailgo->send(); 

			$json['error'] = false; 

			echo json_encode($json);

			break;
		}
		case 'internal':{
			
			$name = (empty(htmlspecialchars($_POST["name"]))) ? "" : htmlspecialchars($_POST["name"]);
			$email = (empty(htmlspecialchars($_POST["email"]))) ? "" : htmlspecialchars($_POST["email"]);
			$city = (empty(htmlspecialchars($_POST["city"]))) ? "" : htmlspecialchars($_POST["city"]);
			$question = (empty(htmlspecialchars($_POST["question"]))) ? "" : htmlspecialchars($_POST["question"]);

			$emailgo= new TEmail; 
			$emailgo->from_email= ''; //email отправителя, можно оставить пустым
			$emailgo->from_name= 'Форма обратной связи internal page'; //Тема сообщения
			$emailgo->to_email= ''; // email получателя
			$emailgo->to_name= $name;


			$emailgo->info = array(
				'name' => $name,
				'email' => $email,
				'city' => $city,
				'question' => $question,	
			);


			$body = "";
			if($emailgo->info['name'] != '') $body .= "Имя: ".$emailgo->info['name']."\n";
			if($emailgo->info['email'] != '') $body .= "Электронная почта: ".$emailgo->info['email']."\n";
			if($emailgo->info['city'] != '') $body .= "Город: ".$emailgo->info['city']."\n";
			if($emailgo->info['question'] != '') $body .= "Вопрос: ".$emailgo->info['question']."\n";
			
			$emailgo->body= $body;
			$emailgo->send(); 

			$json['error'] = false; 

			echo json_encode($json);




			break;
		}
	} 
} else { 
	echo json_encode(array("error" => false)); 
}

function mime_header_encode($str, $data_charset, $send_charset) { 
	if($data_charset != $send_charset)
	$str=iconv($data_charset,$send_charset.'//IGNORE',$str);
	return ('=?'.$send_charset.'?B?'.base64_encode($str).'?=');
}
class TEmail {
	public $from_email;
	public $from_name;
	public $to_email;
	public $to_name;
	public $info = array();
	public $data_charset='UTF-8';
	public $send_charset='windows-1251';
	public $body='';
	public $type='text/plain';

	function send(){
		$dc=$this->data_charset;
		$sc=$this->send_charset;
		$enc_to=mime_header_encode($this->to_name,$dc,$sc).' <'.$this->to_email.'>';
		$enc_city=mime_header_encode($this->city,$dc,$sc);
		$enc_from=mime_header_encode($this->from_name,$dc,$sc).' <'.$this->from_email.'>';
		$enc_body=$dc==$sc?$this->body:iconv($dc,$sc.'//IGNORE',$this->body);
		$headers='';
		$headers.="Mime-Version: 1.0\r\n";
		$headers.="Content-type: ".$this->type."; charset=".$sc."\r\n";
		$headers.="From: ".$enc_from."\r\n";
		return mail($enc_to,'Обратная связь',$enc_body,$headers);
	}

}
?>