'use strict';


// custom  inputs
(function() {

	const _elements = document.querySelectorAll('.js-emptyInput');

	Array.prototype.forEach.call(_elements, function(el){

		el.addEventListener('input', function() {

			if (this.value.length > 0)  {
				this.classList.add('isempty')
			} else {
				this.classList.remove('isempty')
			}
		})
	});

}());

//fixed header
(function () {

	const _header = document.querySelector('.header');

	window.addEventListener('scroll', () => {
		if (window.pageYOffset > 50) {
			_header.classList.add('active')
		} else {
			_header.classList.remove('active')
		}
	})

}());

//form modal
(function () {

	const _formModal = document.querySelector('.js-formModal');
	const _modalClose = document.querySelector('.js-formModalClose');
	const _hiddenInput = document.querySelectorAll('.js-hideModal');

	document.addEventListener('click', ev => {
		if (ev.target === document.getElementById('formModal') ||
			ev.target === _modalClose) {
			ev.preventDefault();
			_formModal.classList.remove('active');
			Array.prototype.forEach.call(_hiddenInput,  el => {
				el.classList.remove('active');
			});
			document.querySelector('#ajaxForm').reset();
		}
	});

	$( document ).ready(function() {
		$("#submForm").click(
			function(e){
				e.preventDefault();
				sendAjaxForm('formModal', 'ajaxForm', 'php/request.php',$(this).attr('name'));
				return false;
			}
		);
	});

	function sendAjaxForm(result_form, ajax_form, url,btn_name) {
		var error = false;
		var data = null;

		var checkedInp = document.querySelector('.checkedInput');
		$("#ajaxForm").find(".requiredInput").each(function(){
			if($(this).val() === ''){
				$(this).addClass('error');

				error = true;
			}
		});
		if (checkedInp) {
			if(!error && checkedInp.checked) {
				data = $("#" + ajax_form).serialize();
				data += "&form_name="+btn_name;
				console.log(data);
				$("#ajaxForm").find(".requiredInput").each(function(){
					$(this).removeClass('error');
				});
				$.ajax({
					url:     url,
					type:     "POST",
					dataType: "json",
					data: data,
					success: function(json) {
						console.log(json);
						if(!json.error){
							_formModal.classList.add('active');
							Array.prototype.forEach.call(_hiddenInput,  el => {
								el.classList.add('active');
							});
							console.log('gg');
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(xhr.status);
						alert(thrownError);
					}
				});
			}
		} else {
			if(!error) {
				data = $("#" + ajax_form).serialize();
				data += "&form_name="+btn_name;
				console.log(data);
				$("#ajaxForm").find(".requiredInput").each(function(){
					$(this).removeClass('error');
				});
				$.ajax({
					url:     url,
					type:     "POST",
					dataType: "json",
					data: data,
					success: function(json) {
						console.log(json);
						if(!json.error){
							_formModal.classList.add('active');
							Array.prototype.forEach.call(_hiddenInput,  el => {
								el.classList.add('active');
							});
							console.log('gg');
						}
					},
					error: function () {
						console.log('nope');
					}
				});
			}
		}

	}
}());